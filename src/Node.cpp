#include "Node.h"

void Node::createCodes(const std::string& code_, std::array<std::string, 256>& encoder)
{
    code = code_;
    
    if (left != nullptr || right != nullptr)
    {
	left->createCodes(code_ + "0", encoder);
	right->createCodes(code_ + "1", encoder);
    }
    else
    {
	encoder[c] = code;
    }
}

/* To get minimal value at the top of priority queue
   comparison sign is inversed, in case of equal frequencies
   nodes are sorted according to symbol
 */
bool operator< (const Node& x, const Node& y)
{
    if (x.getFrequency() != y.getFrequency())
    {
	return x.getFrequency() > y.getFrequency();
    }
    return x.getSymbol() < y.getSymbol();
}


std::ostream& operator<<(std::ostream& out, const Node& x)
{
    out << x.getSymbol() << " : " << x.getFrequency() << " : " << x.getCode();
    return out;
}
