#include "Constants.h"
#include "EncoderFactory.h"
#include "HuffmanEncoder.h"
#include "RLEEncoder.h"

using namespace std;

unique_ptr<BaseEncoder> EncoderFactory::get(const Parser& parser) {
    try {
	if (parser.getCompression() == Compression::rle_e) {
	    return make_unique<Encoders::RLE>();
	}
	else {
	    return make_unique<Encoders::Huffman>();
	}
    }
    catch (const exception& e) {
	std::cerr << "Unable to create encoder. Exit" << std::endl;
	exit(EXIT_FAILURE);
    }
}
