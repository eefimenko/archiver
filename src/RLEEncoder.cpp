#include "BufferedWriter.h"
#include "RLEEncoder.h"
#include "Profile.h"

using namespace std;

namespace Encoders {
    
void RLE::encode(istream& in, ostream& out) {
    unsigned char a,b;
    unsigned char count = 0;
    int length = 0;
    
    LOG_DURATION("Encoding stream", getVerbose());
    printVerbose(cout, "Encoding stream");

    char input_buffer[BUFFER_SIZE];
  
    BufferedWriter writer(out);
    
    while(!in.eof()) {
	in.read(input_buffer, BUFFER_SIZE);
	for (int i = 0; i < in.gcount(); ++i) {
	    length++;
	    b = input_buffer[i];
	    if (count == 0) {
		a = b;
		count++;
		continue;
	    }

	    if (b == a && count < 255) {
		count++;
	    }
	    else {
		writer.put2(count, a);
		a = b;
		count = 1;
	    }
	}
    } 
    
    if (count > 0) {
	writer.put2(count, a);
    }
    writer.flush();
  
    printVerbose(cout, " done: Encoded " + to_string(length) + " symbols\n");
}

void RLE::decode(istream& in, ostream& out) {
    LOG_DURATION("Decoding stream", getVerbose());
    printVerbose(cout, "Decoding stream");
    
    char input_buffer[BUFFER_SIZE];
        
    BufferedWriter writer(out);
    int length = 0;
    
    while(!in.eof()) {
	int input_pos = 0;
	in.read(input_buffer, BUFFER_SIZE);
	
	if (in.gcount()%2 != 0)	{
	    throw logic_error("File corrupted");
	}
	
	while(input_pos < in.gcount()) {
	    unsigned char count = input_buffer[input_pos++];
	    unsigned char a = input_buffer[input_pos++];
	    for (int i = 0; i < count; ++i) {
		writer.put(a);
	    }
	    length += count;
	}
    }
    
    writer.flush();
  
    printVerbose(cout, " done: Decoded " + to_string(length) + " symbols\n");
}
    
} // Encoders
