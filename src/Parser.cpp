#include "Parser.h"

using namespace std;

void Parser::parse(int argc, char** argv) {
    try {
	parseCommandLine(argc, argv);
    }
    catch (invalid_argument& ex) {
	cerr << "Wrong command line option: " << ex.what() << endl << endl;
	printUsage();
	exit(EXIT_FAILURE);
    }
    
    if (getOperation() == Operation::help_e) {
	printHelp();
	exit(EXIT_SUCCESS);
    }
}

void Parser::parseCommandLine(int argc, char** argv) {
    /* If program is run without parameters print help and exit.
       This check also handles case of incorrect passed parameters */
    if (argc <= 1) {
	operation = Operation::help_e;
    }

    /* Parse arguments using getopt function */
    /* By default RLE compression is used */
    int opt = 0;
    static const char *optString = "dxhv";
    
    while ((opt = getopt(argc, argv, optString)) != -1) {
        switch (opt) {
	case 'd':
	    operation = Operation::decode_e;
	    break;
	case 'x':
	    compression = Compression::huffman_e;
	    break;
	case 'v':
	    verbose = true;
	    break;
	case 'h':
	    operation = Operation::help_e;
	    break;
	default:
	    throw invalid_argument("Invalid option");
	    break;
        }
    }

    /* The last parameter after command line parsing is the filename,
       otherwise there is no filename or more than one filename is specified.
       In both cases program prints help and exits  */
    if (operation != Operation::help_e) {
	if (optind == argc - 1)	{
	    inputName = argv[optind];
	}
	else {
	    throw invalid_argument("Invalid filename");
	}
    }
}

void Parser::printUsage() {
    cout << "Usage: archiver [options] filename" << endl << endl;
    cout << "Available options:" << endl;
    cout << "-d - decompress the file" << endl;
    cout << "-x - use Huffman compression" << endl;
    cout << "-v - verbose output" << endl;
    cout << "-h - print this help and exit" << endl;
}

void Parser::printHelp() {
    cout << "Simple archiver program can compress (default) and decompress files." << endl;
    cout << "This program implements 2 types of compression: BLE (default) and Huffman." << endl;
    cout << "Files compressed using RLE and Huffman encoding have .ble and .huf extensions, respectively." << endl << endl;
    cout << "Format used in this program:" << endl;
    cout << "-RLE encoding: byte aligned stream |count_1|char_1|....|count_n|char_n|." << endl;
    cout << "If number of char_i in stream > 255, than it is stored as a sequence of |count_i|char_i|, i.e. 266'a' is stored as |255|'a'|10|'a'|." << endl;
    cout << "-Huffman encoding: number of symbols in encoding(char),  pairs of char,  frequency(int), length of message(int), encoded message." << endl << endl;
    printUsage();
}



