#include <memory>
#include <numeric>
#include <queue>

#include "BufferedWriter.h"
#include "HuffmanEncoder.h"
#include "Profile.h"

using namespace std;

namespace Encoders {
    
void Huffman::encode(istream& in, ostream& out) {
    LOG_DURATION("Total time", getVerbose());
    createEncoder(in);
    writeFrequencies(out);
    writeLength(out);
    encodeStream(in, out);
    clear();
}

void Huffman::decode(istream& in, ostream& out) {
    LOG_DURATION("Total time", getVerbose());
    readFrequencies(in);
    readLength(in);
    decodeStream(in, out);
    clear();
}

void Huffman::buildHuffmanTree() {
    /* Based on frequencies create priority queue,
       such that symbols with minimum frequency is at the top */
    LOG_DURATION(" Build huffman tree", getVerbose());
    priority_queue<shared_ptr<Node>, vector<shared_ptr<Node>>, NodeComp> queue;
    
    for (size_t i = 0; i < 256; ++i) {
	if (freqs[i] > 0) {
	    queue.push(make_shared<Node>((unsigned char)i, freqs[i]));
	}
    }
    
    /*  Main step of Huffman code creation: merge two
	nodes with minimum frequency and insert new Node
	into priority queue. The last Node in queue is the
	root of Huffman tree */
    int n = queue.size();
    for (int i = 0; i < n-1; ++i) {
	shared_ptr<Node> left = queue.top();
	queue.pop();
	shared_ptr<Node> right = queue.top();
	queue.pop();
	queue.push(make_shared<Node>('0', left->getFrequency() + right->getFrequency(), left, right));
    }
    root = queue.top();
}

void Huffman::createEncoder(istream& in)
{
    LOG_DURATION("Create encoder", getVerbose());
    printVerbose(cout, "Creating encoder\n");
    
    {
	LOG_DURATION(" Calculate symbols frequencies", getVerbose());

	char buffer[BUFFER_SIZE];
	while(!in.eof()) {
	    in.read(buffer, BUFFER_SIZE);
	    for (int i = 0; i < in.gcount(); ++i) {
		length++;	
		freqs[(unsigned char)(buffer[i])]++;	
	    }
	}
    }
    
    if (length == 0) {
	return; 
    }

    buildHuffmanTree();
    
    /* To create code representation just traverse the binary tree from the root */
    root->createCodes("", encoder);

    /* Rewind the stream for the second pass on the encoding stage */ 
    in.clear();
    in.seekg(0, in.beg);
}

void Huffman::writeFrequencies(ostream& out) {
    /* Empty file */
    int size = count_if(begin(freqs), end(freqs), [](int x) {return x > 0;});
    if (size == 0) {
	return;
    }
     
    /* First, we need to store the number of symbols in encoding.
       We can have maximum 256 symbols (char) in encoding, but the maximum value
       in unsigned char is 255. To overcome this we take into account that
       number of symbols in encoding >= 1, because file is not empty.
       So we save (count - 1) as number of symbols.
    */
    unsigned char count = (unsigned char) size - 1;
    out.write(reinterpret_cast<const char*>(&count), sizeof(char));
 
    for (size_t i = 0; i < 256; ++i) {
	if (freqs[i] > 0) {
	    out.write(reinterpret_cast<const char*>(&i), sizeof(char)); // symbol
	    out.write(reinterpret_cast<const char*>(&freqs[i]), sizeof(int)); // frequency
	}
    }
    printVerbose(cout, "Number of symbols in encoding: " + to_string(int(count) + 1) + "\n");
}

void Huffman::readFrequencies(istream& in) {
    unsigned char count;
        
    /* Read number of symbols in encoding */
    in.read(reinterpret_cast<char*>(&count), sizeof(char));

    if (in.gcount() == 0) {
	return;
    }
    
    /* For each symbol in encoding read: character and frequency */
    unsigned char c;
    int freq;
    
    for (int i = 0; i < count + 1; ++i) {
	in.read(reinterpret_cast<char*>(&c), sizeof(char)); // symbol
	in.read(reinterpret_cast<char*>(&freq), sizeof(int)); // size of encoding
	
	if (freq <= 0) {
	    throw logic_error("File corrupted");
	}
	freqs[c] = freq;
    }
    
    printVerbose(cout, "Number of symbols in encoding: " + to_string(int(count) + 1) + "\n");
    buildHuffmanTree();
}

void Huffman::writeLength(ostream& out) {
    if (length != 0) {
	out.write(reinterpret_cast<char*>(&length), sizeof(int));
    }
    printVerbose(cout, "Number of characters in file: " + to_string(length) + "\n");
}

void Huffman::readLength(istream& in) {
    in.read(reinterpret_cast<char*>(&length), sizeof(int));
    int count = accumulate(begin(freqs), end(freqs), 0);
    if (count != length) {
	throw logic_error("File corrupted");
    }
    printVerbose(cout, "Number of characters in file: " + to_string(length) + "\n");
}

void Huffman::encodeStream(istream& in, ostream& out) {
    LOG_DURATION("Encode stream", getVerbose());
    
    unsigned char buffer = 0;
    int current_bit = 0;
    int count = 0;
    
    printVerbose(cout, "Encoding stream");
    
    char input_buffer[BUFFER_SIZE];
    BufferedWriter writer(out);
    
    while(!in.eof()) {
	in.read(input_buffer, BUFFER_SIZE);
	
	for (int j = 0; j < in.gcount(); ++j) {
	    string& code = encoder[(unsigned char)input_buffer[j]];
	    
	    for (size_t i = 0; i < code.size(); ++i) {
		if (code[i] == '1') {
		    buffer |= 1 << current_bit;
		}
		current_bit++;
		
		if (current_bit == 8) {
		    writer.put(buffer);
		    current_bit = 0;
		    buffer = 0;
		}
	    }
	    count++;	
	}
    }

    if (current_bit != 0) {
	writer.put(buffer);
    }

    writer.flush();
    
    printVerbose(cout, " done: Encoded " + to_string(count) + " symbols\n");
}

void Huffman::decodeStream(istream& in, ostream& out) {
    LOG_DURATION("Decode stream", getVerbose());
    int count = 0;
    shared_ptr<Node> node = root;

    printVerbose(cout, "Decoding stream");
    
    char input_buffer[BUFFER_SIZE];
    BufferedWriter writer(out);
    
    while (!in.eof()) {
	in.read(input_buffer, BUFFER_SIZE);
	
	for (int i = 0; i < in.gcount(); ++i) {
	    for (int k = 0; k < 8; ++k) {
		node = input_buffer[i] & (1 << k)
		    ? node->getRight()
		    : node->getLeft();

		if (node->isLeaf()) {
		    writer.put(node->getSymbol());
		    node = root;
		    count++;
		    if (count == length) {
			break;
		    }
		}
	    }
	}
    }

    writer.flush();

    printVerbose(cout, " done: Decoded " + to_string(count) + " symbols\n");
}

void Huffman::clear() {
    length = 0;
    encoder.fill("");
    freqs.fill(0);
    destroyHuffmanTree();
}

void Huffman::destroyHuffmanTree() {
    root = nullptr;
}
    
} // Encoders
