#include <memory>

#include "EncoderFactory.h"
#include "Parser.h"

int main(int argc, char** argv) {
    std::ios::sync_with_stdio(false);
       
    Parser parser(argc, argv);
    EncoderFactory encoders;
    
    std::unique_ptr<BaseEncoder> encoder = encoders.get(parser);

    encoder->run(parser);
    
    return EXIT_SUCCESS;
}
