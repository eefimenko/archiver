#include <string>
#include "Constants.h"
#include "BaseEncoder.h"

using namespace std;

void BaseEncoder::run(const Parser& parser) {
    ifstream in;
    ofstream out;
    
    prepareFiles(parser, in, out);
    performOperation(parser, in, out);
}

/* Based on compression method and direction(encode/decode)
   set correct names of input and output files
   compression:    outputName = inputName  + .extension
   uncompression:  inputName  = outputName + .extension
 */
void BaseEncoder::getFilenamesByExtension(const Parser& parser,
					  string& inputName,
					  string& outputName) {
    inputName = parser.getInputName();
    
    if (parser.getOperation() == Operation::encode_e)
    {
	outputName = inputName + "." + getExtension();
    }
    else if (parser.getOperation() == Operation::decode_e)
    {
	size_t lastDot = inputName.rfind('.');
	if (lastDot == string::npos)
	{
	    cerr << "Incorrect extension. For "
		<< getName() << " encoding extension must be ."
		<< getExtension() << endl;
	    exit(EXIT_FAILURE);
	}
	else
	{
	    string extension = inputName.substr(lastDot+1, string::npos);
	    if (extension == getExtension())
	    {
		outputName = inputName.substr(0, lastDot);
	    }
	    else
	    {
		cerr << "Incorrect extension: " << extension << ". For "
			  << getName() << " encoding extension must be ."
			  << getExtension() << endl;
		exit(EXIT_FAILURE);
	    }
	}
    }
}

void BaseEncoder::prepareFiles(const Parser& parser, ifstream& in, ofstream& out) {
    string inputName, outputName;
    getFilenamesByExtension(parser, inputName, outputName);
    in.exceptions(ifstream::badbit);

    in.open(inputName, ios::in | ios::binary);
    
    if (in.fail()) {
	cerr << "Unable to open file " << inputName << " for reading" << endl;
	exit(EXIT_FAILURE);
    }
        
    out.exceptions(ofstream::badbit);

    out.open(outputName, ios::out | ios::binary);
    if (out.fail()) {
	cerr << "Unable to open file " << outputName << " for writing" << endl;
	in.close();
	exit(EXIT_FAILURE);
    }
}

void BaseEncoder::performOperation(const Parser& parser, ifstream& in, ofstream& out) {
    setVerbose(parser.getVerbose());
    
    try {
	if (parser.getOperation() == Operation::encode_e) {
	    encode(in, out);
	}
	else {
	    decode(in, out);
	}
    }
    catch (const ifstream::failure& e) {
	cerr << "IO error" << endl;
	exit(EXIT_FAILURE);
    }
    catch (const out_of_range& e) {
	cerr << "Internal memory is corrupted: " << e.what() << endl;
	exit(EXIT_FAILURE);
    }
    catch (const logic_error& e) {
	cerr << "File format is wrong" << endl;
	exit(EXIT_FAILURE);
    }
}
