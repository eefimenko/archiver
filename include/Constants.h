#pragma once
const int BUFFER_SIZE = 128;
enum class Operation {help_e, encode_e, decode_e};
enum class Compression {rle_e, huffman_e}; 
