#pragma once
#include <memory>

#include "BaseEncoder.h"
#include "Parser.h"

class EncoderFactory
{
public:
    EncoderFactory() = default;
    std::unique_ptr<BaseEncoder> get(const Parser& parser);
};
