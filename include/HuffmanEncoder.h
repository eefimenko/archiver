#pragma once
#include <algorithm>
#include <memory>
#include <vector>

#include "Constants.h"
#include "BaseEncoder.h"
#include "Node.h"

namespace Encoders {

class Huffman : public BaseEncoder
{
public:
    Huffman() : BaseEncoder("Huffman", "huf", false), length(0), root(nullptr) {};
    void encode(std::istream& in, std::ostream& out) override;
    void decode(std::istream& in, std::ostream& out) override;
    void clear() override;
    
private:
    std::array<int, 256> freqs;           // table of frequencies for each char
    std::array<std::string, 256> encoder; // table of encodings for each char
    int length;                           // length of message
    std::shared_ptr<Node> root;                           // root of the Huffman tree
    
    void createEncoder(std::istream& in);
    
    void buildHuffmanTree();
    void destroyHuffmanTree();
        
    void writeFrequencies(std::ostream& out);
    void readFrequencies(std::istream& in);
    
    void writeLength(std::ostream& out);
    void readLength(std::istream& in);
    
    void encodeStream(std::istream& in, std::ostream& out);
    void decodeStream(std::istream& in, std::ostream& out);
};

} // Encoders
