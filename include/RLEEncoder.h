#pragma once

#include "BaseEncoder.h"
#include "Constants.h"

/* Byte-level RLE implementation is based on
   https://www.fileformat.info/mirror/egff/ch09_03.htm */
namespace Encoders {
    
class RLE : public BaseEncoder {
public:
RLE() : BaseEncoder("RLE", "rle", false) {};
    void encode(std::istream& in, std::ostream& out) override;
    void decode(std::istream& in, std::ostream& out) override;
    void clear() override {};
};

} // Encoders
