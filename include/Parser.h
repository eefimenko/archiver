#pragma once
#include <string>
#include <iostream>
#include "getopt.h"
#include "Constants.h"

class Parser
{
public:
    Parser() : operation(Operation::encode_e),
	compression(Compression::rle_e),
	inputName(""),
	verbose(false) {};
    
    explicit Parser(int argc, char** argv) : operation(Operation::encode_e),
	compression(Compression::rle_e),
	inputName(""),
	verbose(false) { parse(argc, argv); };
    
    void parse(int argc, char** argv);
    inline const std::string& getInputName() const {return inputName; };
    inline Operation getOperation() const {return operation; };
    inline Compression getCompression() const {return compression; };
    inline bool getVerbose() const {return verbose; };
       
private:
    void parseCommandLine(int argc, char** argv);
    void printUsage();
    void printHelp();
	
    Operation operation;
    Compression compression;
    std::string inputName;
    bool verbose;
};
