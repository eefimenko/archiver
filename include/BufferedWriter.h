#pragma once

#include <iostream>
#include "Constants.h"

class BufferedWriter {
public:
    explicit BufferedWriter(std::ostream& out) : position_(0), out_(out) {}
    
    inline void put(char c) {
	buffer_[position_++] = (unsigned char)c;
	if (position_ == BUFFER_SIZE) {
	    flush();
	}
    }

    inline void put2(char c1, char c2) {
	buffer_[position_++] = (unsigned char)c1;
	buffer_[position_++] = (unsigned char)c2;
	if (position_ == BUFFER_SIZE) {
	    flush();
	}
    }

    inline void flush() {
	if (position_ > 0) {
	    out_.write(buffer_, position_);
	    position_ = 0;
	}
    }
private:
    char buffer_[BUFFER_SIZE];
    int position_;
    std::ostream& out_;
};
