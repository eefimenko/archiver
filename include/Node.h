#pragma once
#include <array>
#include <iostream>
#include <memory>

class Node
{
public:
    Node(unsigned char c, int freq,
	 std::shared_ptr<Node> left_ = nullptr,
	 std::shared_ptr<Node> right_ = nullptr)
	: c(c), freq(freq), code("")
	, right(right_)
	, left(left_)
	, leaf(left_ == nullptr && right_ == nullptr) {};
    
    inline unsigned char getSymbol() const {return c;};
    inline int getFrequency() const {return freq;};
    inline const std::string& getCode() const {return code;};
    inline std::shared_ptr<Node> getRight() const { return right; };
    inline std::shared_ptr<Node> getLeft() const { return left; };
    inline bool isLeaf() const {return leaf; };
       
    void createCodes(const std::string& code_, std::array<std::string, 256>& encoder);
    
private:
    unsigned char c;
    int freq;
    std::string code;
    std::shared_ptr<Node> right;
    std::shared_ptr<Node> left;
    bool leaf;
};

bool operator< (const Node& x, const Node& y); 
std::ostream& operator<<(std::ostream& out, const Node& x);

class NodeComp {
public:
    bool operator()(std::shared_ptr<Node> lhs, std::shared_ptr<Node> rhs) const
    {
	return *lhs < *rhs;
    }
};
