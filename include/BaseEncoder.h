#pragma once
#include <iostream>
#include <string>
#include <fstream>

#include "Parser.h"

class BaseEncoder
{
public:
    BaseEncoder(const std::string& n, const std::string& ext, bool v)
	: name(n), extension(ext), verbose(v) {};
    virtual ~BaseEncoder() {};

    void run(const Parser& parser);
    virtual void encode(std::istream& in, std::ostream& out) = 0;
    virtual void decode(std::istream& in, std::ostream& out) = 0;
    virtual void clear() = 0;
    inline const std::string& getName() const {return name; };
    inline const std::string& getExtension() const {return extension; };
    inline bool getVerbose() {return verbose; };
    inline void setVerbose(bool v) {verbose = v; };
    inline void printVerbose(std::ostream& out, const std::string& str)
    { if (verbose) out << str << std::flush; };
    
private:
    void getFilenamesByExtension(const Parser& parser, std::string& inputName, std::string& outputName);
    void prepareFiles(const Parser& parser, std::ifstream& in, std::ofstream& out);
    void performOperation(const Parser& parser, std::ifstream& in, std::ofstream& out);
    
    std::string name;
    std::string extension;
    bool verbose;
};
