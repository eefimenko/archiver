# Description
Simple archiver program can compress (default) and uncompress files. 
This program implements 2 types of compression: RLE (default) and static Huffman. 
Files compressed using RLE and Huffman encoding have .rle and .huf extensions, respectively. 

# Output format 
## RLE encoding 
* [char] count_1
* [char] char_1
* ...
* [char] count_n
* [char] char_n

If number of char_i in stream > 255, than it is stored as a sequence of |count_i|char_i|, i.e. string of 511 'a's is stored as |255|'a'|255|'a'|1|'a'|. 
## Huffman encoding 
* [char] number of symbols in encoding 
* [char] char_1
* [int] frequency_1
* ...
* [char] char_n
* [int] frequency_n
* [int] length of message
* encoded message.

# Usage
*  ./archiver <filename>    - compress filename using RLE encoder
*  ./archiver -x <filename> - compress filename using Huffman encoder
*  ./archiver -d <filename> - uncompress filename using RLE encoder
*  ./archiver -xd <filename> - uncompress filename using Huffman encoder

## Additional options
* -v - verbose output
* -h - print help

# Build instructions
Program was tested in Ubuntu 18.04 using gcc 7.4.0 and Windows 10 using Visual Studio 2019.
Cmake is used to build the project.

## Ubuntu 18.04 using gcc + Make
1. Clone repository: 
    git clone git@bitbucket.org:eefimenko/archiver.git
2. Go to project directory: cd archiver
3. Create build directory: mkdir build
4. Go to build directory: cd build
5. Run cmake: cmake ..
6. Compile project using generated Makefile: make
7. To run tests: ./unit_test
8. To run program: ./archiver [options] filename

## Windows 10 using Visual Studio 2019
1. Clone repository: 
    git clone git@bitbucket.org:eefimenko/archiver.git
2. Go to project directory: cd archiver
3. Create build directory: mkdir build
4. Go to build directory: cd build
5. Run cmake: cmake ..
6. Open archiver.sln in Visual Studio 2019 and compile
7. To run tests: .\Release\unit_test
8. To run program: .\Release\archiver [options] filename

# Performance test
Program was tested in Ubuntu 18.04 using 117 Mb file (59 Mb JPEG encoded with RLE). 
Code was built with -O2 optimization. 
Time was measured in three runs and the minimal one was chosen.

Program           | File size (Mb)    | Compress time (s)   | Uncompress time (s)
------------------| ------------------|---------------------|-------------------
archiver RLE      |       233         |   1.0               |    1.0
archiver Huffman  |        72         |   4.6               |    4.2
gzip              |        70         |  11.7               |    1.5
bzip2             |        55         |  17.4               |    8.1
