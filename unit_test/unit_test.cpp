#include <iostream>
#include <sstream>
#include <cassert>
#include <exception>
#include <string>
#include <map>
#include "BaseEncoder.h"
#include "Constants.h"
#include "RLEEncoder.h"
#include "HuffmanEncoder.h"

using namespace std;

string constructStringFromMap(const map<char, int> map) {
    string str;
    for (const auto& p: map) {
	for (int i = 0; i < p.second; ++i) {
	    str.push_back(p.first);
	}
    }
    return str;
}

void testCase(BaseEncoder& encoder, const string& name,
	      Operation op, const string& in, const string& expected, bool twoWay = false) {
    istringstream inputStream(in);
    ostringstream outputStream;
    string result;
    cout << encoder.getName() << " encoder: ";
    
    try {
	if (op == Operation::encode_e) {
	    if (!twoWay) {
		cout << "encoding of " << name << " : ";
		encoder.encode(inputStream, outputStream);
	    }
	    if (twoWay) {
		cout << "encoding/decoding of " << name << " : ";
		encoder.encode(inputStream, outputStream);
		istringstream inputStream1(outputStream.str());
		outputStream.str(string());
		encoder.decode(inputStream1, outputStream);
	    }
	}
	else if (op == Operation::decode_e) {
	    
	    if (!twoWay) {
		cout << "decoding of " << name << " : ";
		encoder.decode(inputStream, outputStream);
	    }
	    if (twoWay) {
		cout << "decoding/encoding of " << name << " : ";
		encoder.decode(inputStream, outputStream);
		istringstream inputStream1(outputStream.str());
		outputStream.str(string());
		encoder.encode(inputStream1, outputStream);
	    }
	}
	else {
	    cout << "Wrong operation" << endl;
	    return;
	}
	result = outputStream.str();
    }
    catch(const logic_error& e) {
	result = "Exception caught: " + string(e.what());
    }
    catch(const exception& e) {
	result = "Exception caught: " + string(e.what());
    }
    
    if (expected != result) {
	cout << "Failed" << endl;
	cout << "Result = " << result << "; Expected = " << expected  << endl;
    }
    else {
	cout << "OK" << endl;
    }
    encoder.clear();
    assert(expected == result);
}

void testRLEEncoder()
{
    Encoders::RLE rle;

    testCase(rle, "empty string", Operation::encode_e, "", "");

    testCase(rle, "empty string", Operation::decode_e, "", "");

    testCase(rle, "simple string",
	     Operation::encode_e, "122333abbccc",
	     string({'\x01', '1', '\x02', '2', '\x03', '3', '\x01', 'a', '\x02', 'b', '\x03', 'c'}));

    testCase(rle, "simple string",
	     Operation::decode_e, string({'\x01', '1', '\x02', '2', '\x03', '3', '\x01', 'a', '\x02', 'b', '\x03', 'c'}),
	     "122333abbccc");

    testCase(rle, "corrupted string (no last symbol)",
	     Operation::decode_e, string({'\x01', '1', '\x02', '2', '\x03', '3', '\x01', 'a', '\x02', 'b', '\x03'}),
	     "Exception caught: File corrupted");

    testCase(rle, "corrupted string (no middle symbol)",
	     Operation::decode_e, string({'\x01', '1', '\x02', '2', '3', '\x01', 'a', '\x02', 'b', '\x03', 'c'}),
	     "Exception caught: File corrupted");

    {
	string str = constructStringFromMap({ {'1', 5}, {'2', 6}, {'a', 256}, {'b', 1}, {'c', 1}});
	
	testCase(rle, "string with long sequence of single character (count=256)",
		 Operation::encode_e, str,
		 string({'\x05', '1', '\x06', '2', '\xFF', 'a', '\x01', 'a', '\x01', 'b', '\x01', 'c'}));

	testCase(rle, "string with long sequence of single character (count=256)",
		 Operation::decode_e, 
		 string({'\x05', '1', '\x06', '2', '\xFF', 'a', '\x01', 'a', '\x01', 'b', '\x01', 'c'}),
		 str);
    }

    {
	string str = constructStringFromMap({ {'1', 5}, {'2', 6}, {'a', 511}, {'b', 1}, {'c', 1}});
	
	testCase(rle, "string with long sequence of single character (count=511)",
		 Operation::encode_e, str,
		 string({'\x05', '1', '\x06', '2', '\xFF', 'a', '\xFF', 'a', '\x01', 'a', '\x01', 'b', '\x01', 'c'}));

	testCase(rle, "string with long sequence of single character (count=511)",
		 Operation::decode_e, 
		 string({'\x05', '1', '\x06', '2', '\xFF', 'a', '\xFF', 'a', '\x01', 'a', '\x01', 'b', '\x01', 'c'}),
		 str);
    }

    testCase(rle, "simple string", Operation::encode_e, "122333abbccc","122333abbccc", true);
    
    testCase(rle, "simple string",
	     Operation::decode_e,
	     string({'\x01', '1', '\x02', '2', '\x03', '3', '\x01', 'a', '\x02', 'b', '\x03', 'c'}),
	     string({'\x01', '1', '\x02', '2', '\x03', '3', '\x01', 'a', '\x02', 'b', '\x03', 'c'}), true);
}

void testHuffmanEncoder()
{
   
    Encoders::Huffman huff;
    
    testCase(huff, "empty string", Operation::encode_e, "", "");

    testCase(huff, "empty string", Operation::decode_e, "", "");
    
    testCase(huff, "simple string", Operation::encode_e, "abbcccc",
	     string({'\x02', // count - 1 -- length of encoding
			 'a', '\x01', '\x00', '\x00','\x00', // frequency of symbol (int)
			 'b', '\x02', '\x00', '\x00','\x00', // frequency of symbol (int)
			 'c', '\x04', '\x00', '\x00','\x00', // frequency of symbol (int)
			 '\x07', '\x00', '\x00','\x00', // length of message (int)
			 '\xe8', '\x03'})); // encoded message
    
    testCase(huff, "simple string", Operation::decode_e, 
	     string({'\x02',
			 'a', '\x01', '\x00', '\x00','\x00',
			 'b', '\x02', '\x00', '\x00','\x00',
			 'c', '\x04', '\x00', '\x00','\x00',
			 '\x07', '\x00', '\x00','\x00',
			 '\xe8', '\x03'}),
	     "abbcccc");

    testCase(huff, "simple string with zero length", Operation::decode_e, 
	     string({'\x02',
			 'a', '\x01', '\x00', '\x00','\x00',
			 'b', '\x02', '\x00', '\x00','\x00',
			 'c', '\x04', '\x00', '\x00','\x00',
			 '\x00', '\x00', '\x00','\x00', // error is here
			 '\xe8', '\x03'}),
	     "Exception caught: File corrupted");

    testCase(huff, "simple string with shorter length", Operation::decode_e, 
	     string({'\x02',
			 'a', '\x01', '\x00', '\x00','\x00',
			 'b', '\x02', '\x00', '\x00','\x00',
			 'c', '\x04', '\x00', '\x00','\x00',
			 '\x00', '\x00', '\x00','\x00', 
			 '\xe8'}), //error is here
	     "Exception caught: File corrupted");
    
    testCase(huff, "simple string with incorrect frequency", Operation::decode_e, 
	     string({'\x02',
		     'a', '\x01', '\x00', '\x00','\x00',
		     'b', '\x02', '\x00', '\x00','\x00',
		     'c', '\x07', '\x00', '\x00','\x00', //error is here
		     '\x00', '\x00', '\x00','\x00', '\xe8'}), 
	     "Exception caught: File corrupted");

    testCase(huff, "empty string", Operation::encode_e, "","", true);
    
    testCase(huff, "empty string", Operation::decode_e, "","", true);

    testCase(huff, "simple string", Operation::encode_e, "122333abbccc","122333abbccc", true);

    {
	string str = constructStringFromMap({ {'a', 1}, {'b', 1}, {'c', 2}, {'d', 3}, {'e', 5}, {'f', 8},
					      {'g', 13}, {'h', 21}, {'i', 34}, {'j', 55}, {'k', 89}, {'k', 144}});
	testCase(huff, "string with Fibonacci frequency distribution", Operation::encode_e, str, str, true);
    }

    {
	string str = constructStringFromMap({ {'a', 2}, {'b', 2}, {'c', 2}, {'d', 2}, {'e', 2}, {'f', 2},
					      {'g', 2}, {'h', 2}, {'i', 2}, {'j', 2}, {'k', 2}, {'k', 2} });
	testCase(huff, "string with equal frequency distribution", Operation::encode_e, str, str, true);
    }
}

int main(int argc, char** argv) {
    testRLEEncoder();
    testHuffmanEncoder();
    return 0;
}
